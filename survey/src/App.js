import React from "react";
import * as Survey from "survey-react";
import survey from "./survey.json";
import "survey-react/survey.css";

function App() {
  let model = new Survey.Model(survey);

  let onComplete = (survey, options) => {
    console.log("Survey results: " + JSON.stringify(survey.data));
  };

  return (
    <div className="App">
      <Survey.Survey model={model} onComplete={onComplete} />
    </div>
  );
}

export default App;
